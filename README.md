# LearningML-docker

This is a docker image containing both learningml-editor (https://gitlab.com/learningml/learningml-editor) and lml-scratch (https://gitlab.com/learningml/lml-scratch-gui).

# How to build a new image

1. Build learningml-editor
2. Build lml-scratch
3. copy both to lml folders editor and scratch
4. Build image: docker build juandalibaba/learningml .


# How to run the image in a container

	# docker run -d -p 8080:80 juandalibaba/learningml
