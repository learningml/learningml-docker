FROM nginx:alpine
#COPY ./nginx/conf.d /etc/nginx/conf.d
COPY ./lml /usr/share/nginx/html
COPY ./config.prod.json /usr/share/nginx/html/editor/assets/config/config.prod.json
